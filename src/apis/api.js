import axios from "axios";

const api = axios.create({
  baseURL: 'http://localhost:8080/'
})

api.interceptors.response.use(
  response => response,
  error => {
    if (error.code === 'ECONNABORTED') {
      // handle request timeout
      alert('Request timed out. Please check the network connection');
    } else {
      // handle other error
      alert('The request failed. Please try again later');
    }
    return Promise.reject(error);
  }
)

export default api