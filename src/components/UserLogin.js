import {useUsers} from "../hooks/useUser";
import {useEffect} from "react";
import {useSelector} from "react-redux";
import {Card} from 'antd'

export const UserLogin = () => {
  const {login} = useUsers();
  const userInfo = useSelector(state => state.user.userInfo);

  useEffect(() => {
    login({username: 'admin', password: '123456'})
  }, [login])
  return (
    <Card>
      <p>user name : {userInfo?.username}</p>
      <p>role : {userInfo?.role}</p>
      <p>phone: {userInfo?.phone}</p>
    </Card>
  )
}