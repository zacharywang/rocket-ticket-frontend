import * as userApi from '../apis/user'
import {useDispatch} from "react-redux";
import {userLogin} from "../reducers/userSlice";


export const useUsers = () => {
  const dispatch = useDispatch()
  const login = async (user) => {
    const response = await userApi.login(user)
    dispatch(userLogin(response.data))
  }

  return {
    login
  }
}