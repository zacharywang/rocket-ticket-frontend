import {createSlice} from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: 'userInfo',
  initialState: {
    userInfo: {}
  },
  reducers: {
    userLogin: (state, action) => {
      state.userInfo = action.payload
    }
  },
})

export default userSlice.reducer;
export const {userLogin} = userSlice.actions;