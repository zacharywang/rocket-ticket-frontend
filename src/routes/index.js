import App from "../App";
import ErrorPage from "../pages/ErrorPage";
import {UserLogin} from "../components/UserLogin";

const routes = [
  {
    path: '/',
    element: <App/>,
    errorElement: <ErrorPage/>,
    children: [
      {
        index: true,
        element: <UserLogin />
      }
    ]
  },

];

export default routes;